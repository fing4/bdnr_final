import copy
import datetime
import json
import os
import threading

from bson import Timestamp
from pymongo import MongoClient

import elastic as ES
from elastic import deleteBulkElastic, saveBulkToElastic

appSettingsFile = (
    "./appsettings."
    + (os.environ["APP_ENV"] if "APP_ENV" in os.environ else "local")
    + ".json"
)
print(">>>>>>> ENVIRONMENT SETTINGS FILE: " + appSettingsFile + "\n")

with open(appSettingsFile, "r") as f:
    appsettings = json.load(f)

runLocal = appsettings["RUN_LOCAL"]
resumeTokenFilename = appsettings["RESUME_TOKEN_FILENAME"]
userMongoDB = appsettings["USER_MONGODB"]
passwordMongoDB = appsettings["PASSWORD_MONGODB"]
clusterMongoDB = appsettings["CLUSTER_MONGODB"]
localReplicaSet = appsettings["CLUSTER_MONGODB_LOCAL"]
dbMongoDB = appsettings["DB_MONGODB"]
watchCollections = appsettings["WATCH_COLLECTIONS"]
pipeline = eval(appsettings["PIPELINE"])
logLocalMongoDBChanges = appsettings["LOG_LOCAL_MONGODB_CHANGES"]
locations = appsettings["LOCATION_IDs"]
index = appsettings["ES_INDEX_NAME"]
indexErr = appsettings["ES_INDEX_ERROR"]
stream2Elastic = appsettings["STREAM2ELASTIC"]
testFileDocsSize = appsettings["TEST_FILE_DOCS_SIZE"]


# Pretty print function of json's
def custom_encoder(obj):
    if isinstance(obj, Timestamp):
        return obj.as_datetime().isoformat()
    elif isinstance(obj, datetime.datetime):
        return obj.isoformat()
    else:
        raise TypeError(f"Object of type '{type(obj)}' is not JSON serializable")


# Stream function to send to Elasticsearch
def streamDoc2Elastic(doc):
    if len(doc) > 1:
        try:
            city, country = next(
                (item[1], item[2])
                for item in locations
                if item[0] == str(doc["locationId"])
            )
        except StopIteration:
            print("Location ID not found")
            city = "NA"
            country = "NA"

        newDoc = {
            "id": doc["_id"],
            "locationId": doc["locationId"],
            "location": doc["location"],
            "parameter": doc["parameter"],
            "value": doc["value"],
            "unit": doc["unit"],
            "dateutc": doc["date"]["utc"],
            "datelocal": doc["date"]["local"],
            "city": city,
            "country": country,
        }
        ES.saveBulkToElastic(index, indexErr, [newDoc], "id")
    else:
        ES.deleteBulkElastic(index, indexErr, [doc])
    return


def getRealDoc(changeObj):
    try:
        match changeObj["operationType"]:
            case "insert":
                changeObj_copy = copy.deepcopy(changeObj)
                # Change ObjectId('xxxxxxxxxxxxxxxxxxx') to '_id': 'xxxxxxxxxxxxxxxxxxx'
                changeObj_copy["fullDocument"]["_id"] = str(
                    changeObj_copy["fullDocument"]["_id"]
                )
                doc = changeObj_copy["fullDocument"]
                # print(
                #     "INSERT\n",
                #     json.dumps(
                #         changeObj_copy["fullDocument"], default=custom_encoder, indent=4
                #     ),
                # )
            case "update":
                changeObj_copy = copy.deepcopy(changeObj)
                # Change ObjectId('xxxxxxxxxxxxxxxxxxx') to '_id': 'xxxxxxxxxxxxxxxxxxx'
                changeObj_copy["fullDocument"]["_id"] = str(
                    changeObj_copy["fullDocument"]["_id"]
                )
                doc = changeObj_copy["fullDocument"]
            case "replace":
                changeObj_copy = copy.deepcopy(changeObj)
                # Change ObjectId('xxxxxxxxxxxxxxxxxxx') to '_id': 'xxxxxxxxxxxxxxxxxxx'
                changeObj_copy["fullDocument"]["_id"] = str(
                    changeObj_copy["fullDocument"]["_id"]
                )
                doc = changeObj_copy["fullDocument"]
            case "delete":
                changeObj_copy = copy.deepcopy(changeObj)
                changeObj_copy["documentKey"]["_id"] = str(
                    changeObj_copy["documentKey"]["_id"]
                )
                doc = changeObj_copy["documentKey"]
        # print("\n")
    except Exception as e:
        print(e)
    return doc


# Create a thread to keep looking for changes
def look_for_changes():
    # while True:
    try:
        # Get the resume token from the file
        with open(resumeTokenFilename, "r") as f:
            resume_token = f.read()

        if (resume_token is None) or (len(resume_token) == 0):
            change_stream = collection.watch(
                full_document="updateLookup", pipeline=pipeline, resume_after=None
            )
        else:
            # Reopen the change stream with the resume token
            change_stream = collection.watch(
                full_document="updateLookup",
                pipeline=pipeline,
                resume_after=eval(resume_token),
            )

        # PROCESS: Iterate over the change stream documents
        docs = []
        print("Watching for changes")

        for change in change_stream:
            try:
                doc = getRealDoc(change)

                if logLocalMongoDBChanges:
                    # Store the changes in the log collection
                    db[watchCollections[0] + "_log"].insert_one(change)

                if stream2Elastic:
                    # Store document to Elasticsearch
                    streamDoc2Elastic(doc)

                # Get the next resume_token
                resume_token = change["_id"]

                # Save the resume token
                with open(resumeTokenFilename, "w") as f:
                    f.write(json.dumps(resume_token))

            except Exception as e:
                print(e)

    except Exception as e:
        print(e)


try:
    change_stream = []
    # Connect to MongoDB
    print("Connecting to MongoDB . . .")
    if runLocal:
        client = MongoClient(localReplicaSet)
    else:
        client = MongoClient(
            f"mongodb+srv://{userMongoDB}:{passwordMongoDB}@{clusterMongoDB}/",
            ssl=True,
            tlsAllowInvalidCertificates=True,
        )
    db = client[dbMongoDB]
    collection = db[watchCollections[0]]
    print("Connected to MongoDB . . .")

    # Start the thread for continuous monitoring changes
    print("Starting monitoring collections changes . . .")
    thread = threading.Thread(target=look_for_changes)
    thread.start()

    # Wait for the Enter key to be pressed
    input("Press Enter to stop...\n")

    # Terminate the script process
    os.kill(os.getpid(), 9)

    # Other shutdown options:
    # Windows: to end Thread execute as admin: taskkill /f /im python.exe
    # Linux:   to end Thread execute as admin: killall -9 python


except Exception as e:
    # print("Connection to Elasticsearch failed:", e)
    print(e)
