import json
import os

import pymongo

appSettingsFile = (
    "./appsettings."
    + (os.environ["APP_ENV"] if "APP_ENV" in os.environ else "local")
    + ".json"
)
print(">>>>>>> ENVIRONMENT SETTINGS FILE: " + appSettingsFile + "\n")

with open(appSettingsFile, "r") as f:
    appsettings = json.load(f)


dbMongoDB = appsettings["DB_MONGODB"]
watchCollections = appsettings["WATCH_COLLECTIONS"]
pipeline = eval(appsettings["PIPELINE"])
# Connection string
clusterMongoDB = appsettings["CLUSTER_MONGODB_LOCAL"]

# Connect to MongoDB
client = pymongo.MongoClient(clusterMongoDB)

db = client[dbMongoDB]
collection = db[watchCollections[0]]
print("Connected to MongoDB . . .")

# # Start change stream
with collection.watch(full_document="updateLookup", pipeline=pipeline) as stream:
    for change in stream:
        # Process change events
        print(change)
