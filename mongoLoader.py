import json
import os

import requests
from bson import Binary, Code
from pymongo import MongoClient

appSettingsFile = (
    "./appsettings."
    + (os.environ["APP_ENV"] if "APP_ENV" in os.environ else "local")
    + ".json"
)
print(">>>>>>> ENVIRONMENT SETTINGS FILE: " + appSettingsFile + "\n")

with open(appSettingsFile, "r") as f:
    appsettings = json.load(f)

runLocal = appsettings["RUN_LOCAL"]
clusterMongoDB = appsettings["CLUSTER_MONGODB"]
localReplicaSet = appsettings["CLUSTER_MONGODB_LOCAL"]
userMongoDB = appsettings["USER_MONGODB"]
passwordMongoDB = appsettings["PASSWORD_MONGODB"]
clusterMongoDB = appsettings["CLUSTER_MONGODB"]
dbMongoDB = appsettings["DB_MONGODB"]
watchCollections = appsettings["WATCH_COLLECTIONS"]
locations = appsettings["LOCATION_IDs"]
metrics = appsettings["METRICS"]
dateIni = appsettings["MONGODB_DATE_INI"]
dateEnd = appsettings["MONGODB_DATE_END"]
limit = appsettings["MONGODB_LIMIT"]

try:
    # Connect to MongoDB
    print("Connecting to MongoDB . . .")
    if runLocal:
        client = MongoClient(localReplicaSet)
    else:
        client = MongoClient(
            f"mongodb+srv://{userMongoDB}:{passwordMongoDB}@{clusterMongoDB}/",
            ssl=True,
            tlsAllowInvalidCertificates=True,
        )
    db = client[dbMongoDB]
    collection = db[watchCollections[0]]
    print("Connected to MongoDB . . .")

    payload = {}
    headers = {}
    for i, location in enumerate(locations):
        for j, metric in enumerate(metrics):
            url = f"""https://api.openaq.org/v2/measurements?date_from={dateIni}&date_to={dateEnd}&parameter={metrics[j]}&location_id={locations[i][0]}&limit={limit}&sort=desc"""
            response = requests.request("GET", url, headers=headers, data=payload)

            json_response = json.loads(response.text)["results"]

            print(f"Loading metric to MongoDB: {metric}, {location[1]} - {location[2]}")
            if len(json_response) != 0:
                collection.insert_many(json_response)
            else:
                print("No data available")
except Exception as e:
    print(e)
