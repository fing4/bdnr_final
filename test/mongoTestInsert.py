import json
import os
from datetime import datetime

from bson import ObjectId
from pymongo import MongoClient

appSettingsFile = (
    "./appsettings."
    + (os.environ["APP_ENV"] if "APP_ENV" in os.environ else "local")
    + ".json"
)
print(">>>>>>> ENVIRONMENT SETTINGS FILE: " + appSettingsFile + "\n")

with open(appSettingsFile, "r") as f:
    appsettings = json.load(f)

runLocal = appsettings["RUN_LOCAL"]
localReplicaSet = appsettings["CLUSTER_MONGODB_LOCAL"]
clusterMongoDB = appsettings["CLUSTER_MONGODB"]
userMongoDB = appsettings["USER_MONGODB"]
passwordMongoDB = appsettings["PASSWORD_MONGODB"]
clusterMongoDB = appsettings["CLUSTER_MONGODB"]
dbMongoDB = appsettings["DB_MONGODB"]
watchCollections = appsettings["WATCH_COLLECTIONS"]
testFileMongoDB = (
    appsettings["MONGODB_JSON_TEST_FILE"]
    + str(appsettings["TEST_FILE_DOCS_SIZE"])
    + ".json"
)


def testInsert():
    try:
        print("Connecting to MongoDB . . .")
        if runLocal:
            client = MongoClient(localReplicaSet)
        else:
            client = MongoClient(
                f"mongodb+srv://{userMongoDB}:{passwordMongoDB}@{clusterMongoDB}/",
                ssl=True,
                tlsAllowInvalidCertificates=True,
            )
        db = client[dbMongoDB]
        collection = db[watchCollections[0]]
        print("Connected to MongoDB . . .")

        # Read documents from JSON file
        with open(testFileMongoDB, "r") as file:
            documents = json.load(file)

        # Convert document _id values to ObjectId
        for document in documents:
            document["_id"] = ObjectId(document["_id"]["$oid"])

        # Insert documents into MongoDB
        collection.insert_many(documents)

        # Print the total number of inserted documents
        print("Total documents inserted:", len(documents))
        input("Press Enter to continue...")

        # Close the connection
        client.close()
    except Exception as e:
        print(e)
        input("Press Enter to continue...")


testInsert()
