import datetime
import json
import os
import sys

from elasticsearch import Elasticsearch, helpers

appSettingsFile = (
    "./appsettings."
    + (os.environ["APP_ENV"] if "APP_ENV" in os.environ else "local")
    + ".json"
)

with open(appSettingsFile, "r") as f:
    appsettings = json.load(f)

logDocs = appsettings["LOG_DOCS"]
runLocal = appsettings["RUN_LOCAL"]

if runLocal:
    es = Elasticsearch([appsettings["ES_URL_LOCAL"]])
else:
    es = Elasticsearch([appsettings["ES_URL_REMOTE"]])


def logES(
    pIndex, pFile, pFunction, pType, pMsg, **kwargs
):  # Get optional parameter "text"
    doc = {
        "datetime": datetime.datetime.now().isoformat(),
        "file": pFile,
        "function": pFunction,
        "type": pType,
        "message": pMsg,
        # Optional parameter named "text", not mandatory
        "text": kwargs.get("text", None),
    }
    print(pType, ": ────> ", doc)
    res = es.index(index=pIndex, id=datetime.datetime.now().isoformat(), document=doc)
    es.indices.refresh(index=pIndex)


def buildId(pJson, args):
    pId = ""
    for ar in args:
        # Each field of _id is cutted down to 140 characters in order to don't exceed 512 bytes of final _id
        pId = pId + str(pJson[ar])[0:140].replace(" ", "")
    return pId


def saveBulkToElastic(pIndex, pIndexError, pJsonArray, *args):
    try:
        print(pJsonArray) if logDocs else None
        if len(args) != 0:
            actions = [
                {
                    "_index": pIndex,
                    "_id": buildId(pJson, args),
                    "_source": pJson,
                    "refresh": True,
                }
                for pJson in pJsonArray
            ]
        else:
            actions = [
                {"_index": pIndex, "_source": pJson, "refresh": True}
                for pJson in pJsonArray
            ]

        helpers.bulk(es, actions=actions, chunk_size=1000)

        es.indices.refresh(index=pIndex)

    except Exception as e:
        print("ERROR:", e)
        logES(
            pIndexError,
            __file__,
            sys._getframe().f_code.co_name,
            "ERROR",
            str(e),
            text="ERROR LOADING TO ELASTICSEARCH",
        )


def deleteBulkElastic(pIndex, pIndexError, pJsonArray):
    try:
        # Get the list of ids to delete
        ids = [item["_id"] for item in pJsonArray]

        # Create a bulk delete request
        delete_requests = []
        for id in ids:
            delete_requests.append({"_op_type": "delete", "_index": pIndex, "_id": id})

        # Execute the bulk delete request
        response = helpers.bulk(es, delete_requests)
        es.indices.refresh(index=pIndex)
        print(f"{response[0]} documents deleted", ids) if logDocs else None
    except helpers.BulkIndexError as e:
        # Handle bulk delete errors
        print(f"{len(e.errors)} document(s) failed to delete:")
        for error in e.errors:
            print(f"- Document ID: {error['delete']['_id']}")
            print(f"  Error reason: {error['delete']['error']['reason']}")
        logES(
            pIndexError,
            __file__,
            sys._getframe().f_code.co_name,
            "ERROR",
            str(e),
            text="ERROR DELETING IN ELASTICSEARCH",
        )
