**<span style="font-size: 1.5em;">
UDELAR - Facultad de Ingeniería<br>
Bases de Datos No Relacionales<br>
Trabajo Final 2023<br>
Grupo 2<br>
Arturo Castagnino / Franco Fontana<br>
</span>**
<span style="font-size: 0.8em;">
<br>

## Descripción

El presente repositorio contiene el material generado para cumplir con los requerimientos de la tarea final del curso “Bases de Datos No Relacionales” de la "Maestría en Ciencia de Datos y Aprendizaje Automático".

Se trata de un servicio de captura de eventos en una base de datos MongoDB utilizando change streams. Los eventos capturados son enviados en tiempo real a una base de datos Elasticsearch.

## Referencias

Indroducción a la Ciencia de Datos y Aprendizaje Automático: https://eva.fing.edu.uy/course/view.php?id=947<br>
